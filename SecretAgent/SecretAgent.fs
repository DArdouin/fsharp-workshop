module SecretAgent

open System
open Rop
open Utils

let nodral : Agent = { 
    FirstName = "Damien"; 
    LastName = "Nod";
    State = Inactive;
    Assignement = None;
}

let ukMission : MissionAssignement<EUR> = {
    Location = "UK";
    Reward = 1000000<EUR>;
    Succeeded = false;
}

let identifyAgent c = 
    printfn "Agent : %s %s" c.FirstName c.LastName 
    c

let analyseAgentState agent =
    analyseAgent
    match agent.State with
    | Banned _ -> printfn "Rogue agent detected !"  
    | _ -> printfn "You're welcome !"
    agent 

let assignMission c = 
    match c.State with 
    | Active -> succeedWithLog {c with Assignement = Some(ukMission)} ("New mission : " + ukMission.Location + "...") 
    | _ -> fail "Sorry, but you are not activated yet, how could we give you a mission ?.."

let executeMission c = 
    printfn "Executing missiong, hold your breath..." 
    printfn "\n....."
    printfn "\n....."
    let succeeded = rdm 0 5 >= 2
    if succeeded then 
        succeed {c with Assignement = Some({c.Assignement.Value with Succeeded = true})}
    else
        fail "You were spotted !"      

let modifyReward (agent : Agent) (modificationFunction : int<EUR> -> int<EUR>) = 
    match agent.Assignement with
    | Some assignment -> Some(modificationFunction assignment.Reward)
    | None _ -> None

let increaseReward agent =  
    let increaser (x : int<EUR>) = x * rdm 1 2 
    modifyReward agent increaser 

let decreaseReward agent =
    let decreaser (x : int<EUR>) = x / rdm 2 5    
    modifyReward agent decreaser

let displayReward (reward : Option<int<EUR>>) =
    match reward with 
    | Some r -> printfn "Your hard work just earned you %i" r
    | None _ -> printfn "Sorry buddy, nothing for you today.."

let maFunc agent =
    agent |> increaseReward |> displayReward

let endMissionWithSuccess agent = 
    endMission agent maFunc

let scenario = 
    identifyAgent
    >> analyseAgentState
    >> activateAgent 
    >> bindR assignMission
    >> bindR executeMission
    >> endMissionWithSuccess 

[<EntryPoint>]
let main argv =
    scenario <| nodral
    Console.ReadKey() |> ignore   
    0 // return an integer exit code
